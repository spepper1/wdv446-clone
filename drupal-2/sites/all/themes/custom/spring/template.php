<?php

function spring_preprocess_page(&$variables){
    
    $page = $variables['page'];
    $sidebar_right = 'one-fourth';
    $sidebar_left = 'one-fourth';
    $content_class = 'one-half';
    $page['tpl_control'] = [];
 
    
    if (empty($page['sidebar-left']) && empty($page['sidebar-right'])){
        $content_class = 'full-width';
    }
    
    else if (!empty($page['sidebar-left']) && empty($page['sidebar-right'])) {
       $sidebar_left = 'one-fourth';
           $sidebar_right = '';
           $content_class = 'three-fourths';
        
    }
    
     else if (!empty($page['sidebar-right']) && empty($page['sidebar-left'])) {
       $sidebar_right = 'one-fourth';
           $sidebar_left = '';
           $content_class = 'three-fourths';
        
    }
  
   
        $variables['page']['tpl_control']['sidebar_left'] = $sidebar_left;
        $variables['page']['tpl_control']['sidebar_right'] = $sidebar_right;
        $variables['page']['tpl_control']['content_class'] = $content_class;
           
    
    /*
    if sidebar-left is occupied 
        add class of one fourth to sidebar left
            add class three quarters to content

    else 
        add class full width to content 
            add class of '' to sidebar-left
            
            Wednesday March 8th :
            1.Add new region [sidebar_right]
            2.Output template 
            a. If sidebar right is populated is populalted all other lyts
            should respond (25%)
            
            Account for a third scenerio of space occupation
            if both sidebars occupied each 25, content 50
            if only right then 25, 75
            if only left then 25, 75
            
            if sidebar left is occupied and sidebar right are occupied {
            add class of one fourth to each
            add content one half 
            }
            else if sidebar left is occupied sidebar right is unoccupied {
            add class of 25 to sidebar left
            add class of 75 to content
            add class of '' to right
            }
            else if sidebar right is occupied sidebar left is unoccupied {
            add class of 25 to sidebar right
            add class of 75 to content 
            add class of '' to left
            }
            else {
            add class full width to content 
            add class of '' to sidebar-left
            add class of '' to sidebar-right
             }
            
            
            
            
    */ 
    
}